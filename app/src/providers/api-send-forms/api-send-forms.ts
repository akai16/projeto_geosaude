import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { VisitInformationPSA } from '../../classes/VisitInformationPsa';
import { Alert, AlertController } from 'ionic-angular';
import { PsaFormProvider } from '../psa-form/psa-form';
import { VisitInformationLIRAa } from '../../classes/VisitInformationLiraa';
import { LiraaFormProvider } from '../liraa-form/liraa-form';

/*
  Generated class for the ApiSendFormsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiSendFormsProvider {

 // -------------- Atributos --------------------------

  private urlServer = 'http://localhost:8080/';
  private endUrlFormPsa = 'psa-form/';
  private endUrlFormLiraa = 'liraa-form/';

  // -------------- Construtor --------------------------

  constructor(
    public http: Http,
    public psaProvider: PsaFormProvider,
    public liraaProvider: LiraaFormProvider) {
    console.log('Hello ApiSendFormsProvider Provider');
  }

// -------------- Métodos --------------------------

  sendFormPsa(formPsaVisited) {
    return new Promise((resolve, reject) => {
      let hds = new Headers();
      hds.append('Content-Type', 'application/json');

      console.log(formPsaVisited);

      this.http.post(this.urlServer + this.endUrlFormPsa, JSON.stringify(formPsaVisited), { headers: hds })
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);

        }, error => {
          reject(error);
        });
    });
  }


  sendAllFormsPsa(formsPsaVisited) {
    return new Promise((resolve, reject) => {
      let hds = new Headers();
      hds.append('Content-Type', 'application/json');

      console.log(formsPsaVisited);

      formsPsaVisited.forEach((value: VisitInformationPSA) => {
        var visitPSAInfo = value.visitInformationPSA;

        this.http.post(this.urlServer + this.endUrlFormPsa, JSON.stringify(visitPSAInfo), { headers: hds })
          .map(res => res.json())
          .subscribe(data => {
            resolve(data);
            // Deleta todos os registros locais.
            this.psaProvider.remove(value.key);

          }, error => {
            reject(error);
          });
      });
    });
  }

  sendAllFormsLiraa(formsLiraaVisited) {
    return new Promise((resolve, reject) => {
      let hds = new Headers();
      hds.append('Content-Type', 'application/json');

      console.log(formsLiraaVisited);

      formsLiraaVisited.forEach((value: VisitInformationLIRAa) => {
        var visitLiraaInfo = value.visitInformationLIRAa;

        this.http.post(this.urlServer + this.endUrlFormLiraa, JSON.stringify(visitLiraaInfo), { headers: hds })
          .map(res => res.json())
          .subscribe(data => {
            resolve(data);
            // Deleta todos os registros locais.
            this.liraaProvider.remove(value.key);

          }, error => {
            reject(error);
          });
      });
    });
  }



  sendFormLiraa(formLiraaVisited) {
    return new Promise((resolve, reject) => {
      let hds = new Headers();
      hds.append('Content-Type', 'application/json');

      this.http.post(this.urlServer + this.endUrlFormLiraa, JSON.stringify(formLiraaVisited), { headers: hds })
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);

        }, error => {
          reject(error);
        });
    });
  }

}
