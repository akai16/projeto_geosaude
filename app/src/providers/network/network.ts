import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Network } from '@ionic-native/network';
import { Events, ToastController } from 'ionic-angular';


@Injectable()
export class NetworkProvider {

  // -------------- Atributos --------------------------
  // Guarda o estado de conexão da rede (offline ou online).
  public status: string;


  // -------------- Construtor --------------------------

  constructor(
    public network: Network,
    public eventCtrl: Events,
    public toast: ToastController) {

    this.status = "online";
  }


  // -------------- Métodos --------------------------


  // Inicializa evento de rede.
  public initializeNetworkEvents(): void {

    // Quando a internet for desligada.
    this.network.onDisconnect().subscribe(() => {
      if (this.status == "online") {
        // Publica evento avisando que a rede está offline.
        this.eventCtrl.publish('network:offline');

        // Altera estado da rede para offline.
        this.status = "offline";
      }


    });

    // Quando a internet for ligada.
    this.network.onConnect().subscribe(() => {
      if (this.status === "offline") {

        if (this.network.type === 'wifi' || this.network.type == "3g" || this.network.type == "4g") {
          // Publica evento avisando que a rede está online.
          this.eventCtrl.publish('network:online');
          // Altera estado da rede para online.
          this.status = "online";
        }

      }

    });
  }

  // Retorna o estado da rede.
  public getNetworkStatus() {
    return this.status;
  }



}
