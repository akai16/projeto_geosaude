import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { AboutPage } from '../pages/about/about';
import { NetworkProvider } from '../providers/network/network';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  // -------------- Atributos --------------------------

  public newFormsToSend: boolean;                         // Variável que controla quando há novos formulários para serem enviados ao servidor.
  rootPage: any = LoginPage;
  @ViewChild(Nav) nav: Nav;
  pages: Array<{ title: string, component: any }>;        // Criando array para armazenar os componentes que aparecerão no menu

  // -------------- Construtor -----------------------------

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public networkProvider: NetworkProvider) {

    this.newFormsToSend = false;      
    this.initializeApp();

    this.pages = [
      { title: 'Sobre', component: AboutPage }
    ]


  }


  // -------------- Métodos -----------------------------

  initializeApp() {
    this.platform.ready().then(() => {
      this.networkProvider.initializeNetworkEvents();     // Inicia os eventos para monitoramento da conexão com a internet.
      
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // this.nav.setRoot(page.component);
  }

  // Alterar o estado da variável newFormsToSend.
  setNewFormsToSend(val: boolean): void {
    this.newFormsToSend = val;
  }

  // Informar o valor da variável newFormsToSend.
  getNewFormsToSend(): boolean {
    return this.newFormsToSend;
  }
}
