import { Location } from "./location-model";
import { AgentModel } from "./agent-model";

export class Report {

    constructor(public date: Date, public location: Location, public agent: AgentModel){
    }



}
