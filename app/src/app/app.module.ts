import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TooltipsModule } from 'ionic-tooltips'
import { MyApp } from './app.component';

// ------------- Importando Pages -----------------------------------------------------------
import { LoginPage } from '../pages/login/login';
import { PerfilPage } from '../pages/perfil/perfil';
import { AchievementsPage } from '../pages/achievements/achievements';
import { MapPage } from '../pages/map/map';
import { TabsPage } from '../pages/tabs/tabs';
import { ImmobilePage } from '../pages/immobile/immobile';
import { ActivitiesPage } from '../pages/activities/activities';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MapModalPage } from '../pages/map-modal/map-modal';
import { AboutPage } from '../pages/about/about';
import { AedesPage } from '../pages/aedes/aedes';
import { CulexPage } from '../pages/culex/culex';
import { LarvicidesPage } from '../pages/larvicides/larvicides';
import { TratamentoPsaPage } from '../pages/tratamento-psa/tratamento-psa';
import { SelectFormPage } from '../pages/select-form/select-form';
import { StartFormsPage } from '../pages/start-forms/start-forms';
import { TrashPage } from '../pages/trash/trash';
import { StartVisitPage } from '../pages/start-visit/start-visit';

import { DatePipe } from '@angular/common';

// ------------- Importando Providers -----------------------------------------------------------
import { ApiProvider } from '../providers/api/api';
import { PsaFormProvider } from '../providers/psa-form/psa-form';
import { LiraaFormProvider } from '../providers/liraa-form/liraa-form';
import { NetworkProvider } from '../providers/network/network';
import { ApiSendFormsProvider } from '../providers/api-send-forms/api-send-forms';


// ------------- Importando Plugins -----------------------------------------------------------
import { Camera } from '@ionic-native/camera';
import { Network } from '@ionic-native/network';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';



@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    PerfilPage,
    AchievementsPage,
    MapPage,
    TabsPage,
    ImmobilePage,
    MapModalPage,
    AboutPage,
    AedesPage,
    CulexPage,
    LarvicidesPage,
    TratamentoPsaPage,
    SelectFormPage,
    StartFormsPage,
    TrashPage,
    ActivitiesPage,
    StartVisitPage
  ],
  imports: [
    BrowserModule,
    TooltipsModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    PerfilPage,
    AchievementsPage,
    MapPage,
    TabsPage,
    ImmobilePage,
    MapModalPage, 
    AboutPage,
    AedesPage,
    CulexPage,
    LarvicidesPage,
    TratamentoPsaPage,
    SelectFormPage,
    StartFormsPage,
    TrashPage,
    ActivitiesPage,
    StartVisitPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider,
    PsaFormProvider,
    DatePipe,
    LiraaFormProvider,
    Camera,
    ApiSendFormsProvider,
    Network,
    NetworkProvider
  ]
})
export class AppModule {}
