import { VtAedes } from "./VisitInformationPsa";

// VisitInformationLIRAa contém a estrutura que será utilizada para armazenar informações no storage.


export class VisitInformationLIRAa {
  visitInformationLIRAa: any;
  key: any;

  constructor(
    private places: string,                         // Logradouro
    private immobile: ImmobileLIRAa,                // Imóvel
    private vtAedes: BreendingGroundsAedes,         // Vetor aedes. Neste caso (LIRAa), ele só precisará das informações sobre os criadouros.
    private depositsRemoved: number,                // Quantidade de depósitos eliminados
    private larvicides: TpLarvicidesLIRAa           // Larvicidas 
    ) {}


    /**
     * setPlaces
     */
    public setPlace(place: string) {
      this.places = place;      
    }

    /**
     * setImmogile
     */
    public setImmobile(immobile: ImmobileLIRAa) {
      this.immobile = immobile;
    }

    /**
     * setVtAedes
     */
    public setVtAedes(vtAedes: BreendingGroundsAedes) {
      this.vtAedes = vtAedes;
    }

    /**
     * setDepositsRemoved
     */
    public setDepositsRemoved(deposits: number) {
      this.depositsRemoved = deposits;
    }

    /**
     * setLarvicides
     */
    public setLarvicides(tpLarvicides: TpLarvicidesLIRAa) {
      this.larvicides = tpLarvicides;
    }
  }
  
  
  // -------------------------------------------------------------------------------------
  
  export class ImmobileLIRAa {
    tpImmobile: number;
    dateVisit: string;
  }
  /*
  export class VtAedes {
    tpBreedingGrounds: BreendingGroundsAedes    // Tipo de criadouros
  }
  */
  export class BreendingGroundsAedes {
    A1: number;
    A2: number;
    B: number;
    C: number;
    D1: number;
    D2: number;
    E: number;
  }
  
  export class TpLarvicidesLIRAa {
    BTiG_gramas: number;
    BTiG_depositos: number;
  
    BTiWDg_gramas: number;
    BTiWDg_depositos: number;
  }
  
  