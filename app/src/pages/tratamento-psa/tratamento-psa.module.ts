import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TratamentoPsaPage } from './tratamento-psa';

@NgModule({
  declarations: [
    TratamentoPsaPage,
  ],
  imports: [
    IonicPageModule.forChild(TratamentoPsaPage),
  ],
})
export class TratamentoPsaPageModule {}
