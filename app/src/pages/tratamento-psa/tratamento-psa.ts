import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { VtAedes, Treatment, VtCulex, VisitInformationPSA } from '../../classes/VisitInformationPsa';


@IonicPage()
@Component({
  selector: 'page-tratamento-psa',
  templateUrl: 'tratamento-psa.html',
})

export class TratamentoPsaPage {

  // --------------- Atributos -----------------------
  private focusType: string;
  private vtAedesInformation: VtAedes;
  private vtCulexInformation: VtCulex;
  private tpTreatment: Treatment;
  private visitInformation: VisitInformationPSA;


  // -------------- Métodos ------------------------
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {

    // Instância de tratamento, onde será guardado as informações preenchidas para o tratamento no formulário PSA.
    this.tpTreatment = new Treatment();

    this.visitInformation = this.navParams.get('visitInformation');
    console.log(this.visitInformation);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TratamentoPsaPage');
  }

  ionViewWillEnter() {
    this.focusType = this.navParams.get("foco");
  }

  GoBack() {
    this.navCtrl.pop();
  }


  Finish() {

    // Retirar as páginas anteriores da pilha e voltar para a StartFormsPage.
    // É verificado qual o tipo de displayName que será passado como parâmetro, de forma a ajudar no distinção de qual objeto armazenar no VisitInformation.

    if (this.focusType == "Aedes") {        // Foco do tipo Aedes.

      // Ao terminar o preenchimento das informações, será retornado para a página StartForms juntamente com o objeto vtAedesInformation.
      this.vtAedesInformation = new VtAedes();                                                       // Instância de um novo objeto vtAedesInformation.
      this.vtAedesInformation.tpBreedingGrounds = this.navParams.get('breendingGroundAedes');        // Atribuindo o parâmetro de tipos de criadouros.
      this.vtAedesInformation.tpTreatment = this.tpTreatment;                                        // Atribuindo as informações sobre o tratamento.

      
      this.visitInformation.setVtAedes(this.vtAedesInformation);
      console.log(this.visitInformation);

      //this.navCtrl.setRoot(StartFormsPage, { pageFlag: "false", formName: "PSA", visitInformation: this.visitInformation, fieldAedesDone: true });

      this.events.publish('fieldAedesDone', this.visitInformation);
      //this.events.publish('finalizeAedesPage');
      


    } else {                                // Foco do tipo Culex.

      // Ao terminar o preenchimento das informações, será retornado para a página StartForms juntamente com o objeto vtCulexInformation.
      this.vtCulexInformation = new VtCulex();                                                      // Instância de um novo objeto vtCulexInformation.
      this.vtCulexInformation.tpBreedingGrounds = this.navParams.get('breendingGroundCulex');       // Atribuindo o parâmetro de tipos de criadouros.
      this.vtCulexInformation.tpTreatment = this.tpTreatment;                                       // Atribuindo as informações sobre o tratamento.

      this.visitInformation.setVtCulex(this.vtCulexInformation);

      console.log(this.visitInformation);

      //this.navCtrl.setRoot(StartFormsPage, { pageFlag: "false", formName: "PSA", visitInformation: this.visitInformation, fieldCulexDone: true });

      this.events.publish('fieldCulexDone', this.visitInformation);
    }

    //this.navCtrl.popToRoot();
    this.navCtrl.pop();
  }


  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.navCtrl.pop();
  }
}
