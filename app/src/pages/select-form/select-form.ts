import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StartFormsPage } from '../start-forms/start-forms';

/**
 * Generated class for the SelectFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select-form',
  templateUrl: 'select-form.html',
})
export class SelectFormPage {
  //------------ Atributos ----------------------------
  
  streetName: string;
  address: string

  //------------- Métodos ---------------------------
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    // Recebendo o parâmetro passado - nome da rua.
    this.streetName = this.navParams.get("streetName");
  }

  // Abrindo a página de StartForms e passando o nome do formulário escolhido (Liraa ou PSA).
  GotoStartFormsPage(formType: string) {

    // Fazendo a concatenção do nome da rua com o endereço do imóvel, para ser armazenado 
    // Retira virgulas de address afim de normaliza-lo
    let safeAddress = this.address.replace(',',' ')

    // Variavel auxiliar para preservar o endereço original
    let wholeAddress = this.streetName + ', ' +  safeAddress;

    console.log("Endereço concatenado: " + wholeAddress);

    this.navCtrl.push(StartFormsPage, { formName: formType, address: wholeAddress });
  }

}
