/*
*   Geo-Saúde: Tela de Perfil
*   Desenvolvedores: Wellington Pinheiro, Anderson Felix, Gabriel Gadelha, Leydson Barros e Ana Carolina.
*
*/


import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup } from "@angular/forms";
import { Camera } from '@ionic-native/camera';


@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})


export class PerfilPage {
  @ViewChild('fileInput') fileInput;
  usuario ={};
  form: FormGroup;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, formBuilder: FormBuilder, public camera: Camera) {
    
    // Inicializa o objeto que guardará a imagem.
    this.form = formBuilder.group({
      image: ['']
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
    
  }

  // Recebendo os parâmetros.
  ionViewWillEnter()
  {
    this.usuario['nome'] = this.navParams.get('nome');
    this.usuario['profissao'] = this.navParams.get('profissao');
    this.usuario['posto'] = this.navParams.get('posto');
  }


  // Método para obter a imagem.

  getPicture() {
    if( Camera['installed']()) {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 96,
        targetHeight: 96
      }).then((data) => {
        this.form.patchValue({'image': 'data:image/jpg;base64, ' + data});
      }, (err) => {
        alert('Unable to take photo!');
      })
    } else {
      this.fileInput.nativeElement.click();
    }
  }


  // Carregando a imagem.

  processWebImage(event) {
    let reader = new FileReader();
    reader.onload = (readerEvent) => {

      let imageData = (readerEvent.target as any).result;
      this.form.patchValue({'image': imageData});
    };

    reader.readAsDataURL(event.target.files[0]);
  }


  // Carregando o estilo da imagem.

  getProfileImageStyle() {
    return 'url(' + this.form.controls['image'].value + ')';
  }

}
