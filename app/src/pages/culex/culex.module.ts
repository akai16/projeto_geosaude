import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CulexPage } from './culex';

@NgModule({
  declarations: [
    CulexPage,
  ],
  imports: [
    IonicPageModule.forChild(CulexPage),
  ],
})
export class CulexPageModule {}
