import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { StartFormsPage } from '../start-forms/start-forms';
import { TpLarvicides } from '../../classes/VisitInformationPsa';
import { TpLarvicidesLIRAa } from '../../classes/VisitInformationLiraa';


@IonicPage()
@Component({
  selector: 'page-larvicides',
  templateUrl: 'larvicides.html',
})
export class LarvicidesPage {
  // ----------- Atributos -------------

  private formType: string;
  private larvicidesInformation;
  private visitInformation;

  // ------------ Métodos ---------------

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {

    this.formType = this.navParams.get("formType");

    // Instânciando objeto de acordo com o tipo do formulário.
    if (this.formType == 'PSA') {
      this.larvicidesInformation = new TpLarvicides();

    } else {
      this.larvicidesInformation = new TpLarvicidesLIRAa();
    }

    this.visitInformation = this.navParams.get('visitInformation');
    console.log(this.visitInformation);

  }

  ionViewDidLoad() {
  }


  ionViewWillEnter() {

    console.log("tratamento formtype: " + this.formType);
  }

  /*
  GoBack(){
    this.navCtrl.pop();
  }
  */



  Finish() {

    this.visitInformation.setLarvicides(this.larvicidesInformation);
    console.log(this.visitInformation);

    //Retira as páginas anteriores da pilha e volta para a TabsPage.
    //this.navCtrl.setRoot(StartFormsPage, { pageFlag: "false", formName: this.formType, visitInformation: this.visitInformation, fieldLarvicidesDone: true });
    //this.navCtrl.popToRoot();
    this.events.publish('fieldLarvicidesDone', this.visitInformation);
    this.navCtrl.pop();
  }

}
