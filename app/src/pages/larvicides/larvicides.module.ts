import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LarvicidesPage } from './larvicides';

@NgModule({
  declarations: [
    LarvicidesPage,
  ],
  imports: [
    IonicPageModule.forChild(LarvicidesPage),
  ],
})
export class LarvicidesPageModule {}
