import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { StartFormsPage } from '../start-forms/start-forms';
import { Trash, VisitInformationPSA } from '../../classes/VisitInformationPsa';

/**
 * Generated class for the TrashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trash',
  templateUrl: 'trash.html',
})
export class TrashPage {
  private formType: string;
  private trashInformation: Trash;
  private visitInformation: VisitInformationPSA;

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {
    this.trashInformation = new Trash();
    this.visitInformation = this.navParams.get('visitInformation');
    console.log(this.visitInformation);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrashPage');

    //Recebe informação sobre o tipo de formulario selecionado: LIRAa ou PSA.
    this.formType = this.navParams.get("formType");
  }


  // Método que finalizar o preenchimento das informações de lixo

  GotoFinalizeVisit() {

    this.visitInformation.setTrash(this.trashInformation);
    console.log("------ TrashPage:   ");
    console.log(this.visitInformation);
    //Retira as páginas anteriores da pilha e volta para a TabsPage.
    //this.navCtrl.setRoot(StartFormsPage, { formName: this.formType, visitInformation: this.visitInformation, pageFlag: "false", fieldTrashDone: true });
    //this.navCtrl.popToRoot();

    this.events.publish('fieldTrashDone', this.visitInformation);
    this.navCtrl.pop();
  }

}
