import { Component } from '@angular/core';
import { PerfilPage } from '../perfil/perfil';
import { AchievementsPage } from '../achievements/achievements';
import { ActivitiesPage } from '../activities/activities';
import { MapPage } from '../map/map';
import { NavParams } from 'ionic-angular';



@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {


  tab1Root = PerfilPage;
  tab2Root = AchievementsPage;
  tab3Root = ActivitiesPage;
  tab4Root = MapPage;
  loginData;



  constructor(public navParams:NavParams) {
    this.navParams = navParams;
    this.loginData = this.navParams.data;
  }

}
