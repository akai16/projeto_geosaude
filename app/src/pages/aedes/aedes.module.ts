import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AedesPage } from './aedes';

@NgModule({
  declarations: [
    AedesPage,
  ],
  imports: [
    IonicPageModule.forChild(AedesPage),
  ],
})
export class AedesPageModule {}
