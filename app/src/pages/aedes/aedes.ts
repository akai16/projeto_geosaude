import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { TratamentoPsaPage } from '../tratamento-psa/tratamento-psa'
import { BreendingGroundsAedes, VisitInformationPSA } from '../../classes/VisitInformationPsa';


@IonicPage()
@Component({
  selector: 'page-aedes',
  templateUrl: 'aedes.html',
})

export class AedesPage {

  // ------------ Atributos ---------------
  private formType: string;
  private breendingGroundAedes: BreendingGroundsAedes;
  private visitInformation;

  // ----------- Métodos -------------------

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {

    this.breendingGroundAedes = new BreendingGroundsAedes();
    this.visitInformation = this.navParams.get('visitInformation');
    console.log(this.visitInformation);

  }


  // Método executado no carregamento desta tela (AedesPage).

  ionViewDidLoad() {
    console.log('ionViewDidLoad AedesPage');

    // Obtendo o tipo do formulário que está sendo preenchido.
    this.formType = this.navParams.get("formType");
  }


  // Método para chamar a próxima tela.

  GotoNextPage() {
  
    console.log(this.visitInformation);
    this.navCtrl.push(TratamentoPsaPage, { foco: "Aedes", visitInformation: this.visitInformation, breendingGroundAedes: this.breendingGroundAedes });
  }


  // Método para volta a tela anterior.

  GoBacktoPageImovel() {
    this.navCtrl.pop()
  }



  // Finaliza o preenchimento devido o formulário ser LIRAa.


  Finish() {

    // Atribui o objeto com os dados de entrada ao objeto do VisitInformationLIRAa.
    this.visitInformation.setVtAedes(this.breendingGroundAedes);

    console.log(this.visitInformation);
    // Retorna a página StartForms.
    //this.navCtrl.setRoot(StartFormsPage, { pageFlag: "false", formName: this.formType, visitInformation: this.visitInformation, fieldAedesDone: true });
    //this.navCtrl.popToRoot();
    this.events.publish('fieldAedesDone', this.visitInformation);
    this.navCtrl.pop();
  }


}
