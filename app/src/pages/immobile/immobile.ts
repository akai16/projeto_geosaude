import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Immobile } from '../../classes/VisitInformationPsa';
import { ImmobileLIRAa } from '../../classes/VisitInformationLiraa';
import { Events } from 'ionic-angular';

/**
 * Generated class for the ImmobilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-immobile',
  templateUrl: 'immobile.html',
})
export class ImmobilePage {
  // ---------------- Atributos -------------

  private formType: string;
  private date: string;
  private visitInformation;
  private immobileInformation;

  // ---------------- Métodos ---------------

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {
    this.visitInformation = this.navParams.get("visitInformation");
    console.log(JSON.stringify(this.visitInformation));

  }

  ionViewDidLoad() {
    //Recebe informação sobre o tipo de formulario selecionado: LIRAa ou PSA.
    this.formType = this.navParams.get("formType");

    // Instância objeto de acordo com o tipo do formulário.
    if (this.formType == 'PSA') {
      this.immobileInformation = new Immobile();

    } else {
      this.immobileInformation = new ImmobileLIRAa();
    }
  }


  // Volta para a tela StartForms.


  GotoBackSelectFormPage() {
    this.navCtrl.pop()
  }



  // Método que finalizar a visita


  GotoFinalizeVisit() {
    this.immobileInformation.dateVisit = this.date;
    //this.visitInformation.immobile =  this.immobileInformation;
    this.visitInformation.setImmobile(this.immobileInformation);

    
    console.log("---- ImmobilePage:    ");
    console.log(JSON.stringify(this.visitInformation));


    //Retira as páginas anteriores da pilha e volta para a StartForms.
    //this.navCtrl.setRoot(StartFormsPage, { formName: this.formType, pageFlag: "false", visitInformation: this.visitInformation, fieldImmobileDone: true });
    //this.navCtrl.popToRoot();

    this.events.publish('fieldImmobileDone', this.visitInformation);   // Publicando um evento para informar que campo imóvel foi concluído.

    this.navCtrl.pop();
  }

}
