import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SelectFormPage } from '../select-form/select-form';

/**
 * Generated class for the StartVisitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-start-visit',
  templateUrl: 'start-visit.html',
})
export class StartVisitPage {

  items = [
    'Rua Marquês de Baipendi',
    'Rua Cirilino Afonso de Melo',
    'Rua Mário Sete',
    'Rua São Caetano',
    'Av. Agamenon Magalhães',
    'Rua Guaianazes',
    'Rua Esberard',
    'Rua Pereira Passos',
    'Rua Nova',
    'Rua Projetada',
    'Rua Ledinha'
  ];

  items2 = {
    sanitaryDistrict: "IV",
    strata: [
      "Ilha do Retiro e Madalena",
      "Cordeiro 1 e Zumbi",
      "Cordeiro 2",
      "Torre",
      "Prado",
      "Torrôes",
      "Engenho do Meio",
      "Iputinga 1",
      "Várzea 1 (CDU) / Várzea (Brasilit) / CDU",
      "Várzea 3 (Baixo) / Várzea 4 (Barreiras)",
      "Várzea 5 (UR-7)",
      "Caxangá",
      "Iputinga 2"
    ]
  };

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log("start-visit Page Loaded");
  }

  itemSelected(item: string) {
    console.log("Selected Item", item);
  }

  GotoSelectForm(item: string) {
    console.log("Nome da rua escolhida : " + item);
    this.navCtrl.push(SelectFormPage, { streetName: item });
  }

}
