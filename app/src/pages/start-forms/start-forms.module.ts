import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StartFormsPage } from './start-forms';

@NgModule({
  declarations: [
    StartFormsPage,
  ],
  imports: [
    IonicPageModule.forChild(StartFormsPage),
  ],
})
export class StartFormsPageModule {}
