import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { ApiProvider } from '../../providers/api/api'


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [
    ApiProvider
  ]
})

export class LoginPage {

  //---------------------------------------------------
  //    Atributos
  //---------------------------------------------------

  //Dados do Usuario
  public dados: any = {};
  public loadingMsg;



  //---------------------------------------------------
  //    Construtor
  //---------------------------------------------------

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private apiProvider: ApiProvider,
    public loading: LoadingController,
    public toastCtrl: ToastController) {
  }

  //---------------------------------------------------
  //    Métodos
  //---------------------------------------------------

  // Informa via console que estamos na tela de login.
  ionViewDidLoad() {
  }

  // Tentativa de autenticação do Usuário(Login).
  tryToAuthenticate() {
    this.showLoader();

    console.log("Dados:  ");
    console.log(this.dados['username'] + "   " + this.dados['senha']);
    
    // Fazendo a autenticação.
    this.apiProvider.sendAuthenticationData(this.dados['username'], this.dados['senha']).subscribe(
      data => {
        console.log(data)
        this.loadingMsg.dismiss();
        this.showMessage('Login Realizado com Sucesso', 2500);;

        // Coleta dados enviados pelo Back-end.
        const response = (data as any);
        const objeto_JSON = JSON.parse(response._body);

        //Token de Autenticação
        this.apiProvider.setAuthenticationToken(objeto_JSON.auth_token).then((result) => { console.log("AuthToken Armazenado: " + result); });

        //Preenchimento de dados relacionados ao Agente de Saude vindos do JSON
        this.dados.nome = objeto_JSON.nome;
        this.dados.profissao = objeto_JSON.profissao;
        this.dados.posto = objeto_JSON.posto;
        this.dados.metas = objeto_JSON.metas;
        this.dados.visitas_rank = objeto_JSON.visitas;

        //Em caso de sucesso, vá para a pagina de Perfil
        this.GotoTabsPage();
      }
      // Caso contrário, tentar novamente.
      , error => {
        console.log('Login Invalido');
        console.log(error);
        this.showMessage('Login Inválido', 2500);
        return;
      }
    );


  }


  // Abre a página de perfil e passa os parâmetros.
  GotoTabsPage() {
    this.navCtrl.push(TabsPage, {
      'nome': this.dados.nome,
      'profissao': this.dados.profissao,
      'posto': this.dados.posto,
      'metas': this.dados.metas,
      'visitas': this.dados.visitas_rank,
    })
  }


  //Função para Desabilitar Autenticação
  GotoTabsPageNoAuthentication() {
    this.navCtrl.push(TabsPage, {
      'nome': "Luciana Silva",
      'profissao': "Agente de Saúde",
      'posto': "Posto de Saúde da Várzea",
      'metas': {
        'numVisitas': 100,
        'numVisitasFeitas': 43,
        'dataLimite': '2018-01-01',
      },
      'visitas':[
        {'bairro': 'Ibura', 'visitasFeitas': 12},
        {'bairro': 'Derby', 'visitasFeitas': 19},
        {'bairro': 'Pina', 'visitasFeitas': 3}
      ],
    })
  }


  showLoader(){
    this.loadingMsg = this.loading.create({
      content: 'Aguarde...'
    })
  }


  showMessage(message: string, duration?: number) {
    let mess = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: 'bottom'
    });
    mess.present();
  }



}
