from django import forms
from .models import AgenteDeSaude, Metas
from django.contrib.auth import forms as authform

class UserEditForm(forms.ModelForm):
	class Meta:
		model = AgenteDeSaude
		fields = ['nome','profissao','posto','regiao']
		

class CadastroForm(authform.UserCreationForm):
	class Meta:
		model = AgenteDeSaude
		fields = ['username','nome','profissao','posto','regiao']


class MetasForm(forms.ModelForm):
	class Meta:
		model = Metas
		fields = ['dataLimite','numVisitas']
		widgets = {
			'dataLimite' :forms.DateInput(),
		}


class ConsolidadosForm(forms.Form):
	dataInicial = forms.DateField(
		label='Data Inicial', localize=True,
		error_messages={'invalid': "Informe uma data válida no formato dd/mm/aaaa."}
	)
	dataFinal = forms.DateField(
		label='Data Final', localize=True,
		error_messages={'invalid': "Informe uma data válida no formato dd/mm/aaaa."}
	)