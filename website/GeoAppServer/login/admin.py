from django.contrib import admin
from .models import Metas, AgenteDeSaude

@admin.register(AgenteDeSaude)
class AgenteAdmin(admin.ModelAdmin):
    list_display = ('username', 'nome')


@admin.register(Metas)
class MetasAdmin(admin.ModelAdmin):
    list_display = ("idAgente", "get_nomeAgente", "numVisitas", "numVisitasFeitas", "dataCriação", "dataLimite")
    
    def get_nomeAgente(self,obj):
        return obj.idAgente.nome
    get_nomeAgente.short_description = 'Agente de Saúde'

