import xlsxwriter
from datetime import datetime
from api.models import VisitaFormularioLira
from django.db.models import Sum,Q


## DOCUMENTAÇÃO DA BIBLIOTECA
# http://xlsxwriter.readthedocs.io 


def createConsolidados(workbook,DISTRITO_SANITARIO,NUMERO_ESTRATOS,DATA_INICIAL,DATA_FINAL):

    #A Planilha dos Consolidados consiste de 2 Tabelas com cabeçalhos diferente;

    #Não temos informações sobre o albopictus, logo
    #campos relacionados ao Albopictus terão 0 como padrão;

    #Worksheet.merge_range() produz celulas retangulares que abrangem mais de uma linha/coluna;

    #Criando estilos visuias paras as celulas que serão usados na planilha
    header_format = workbook.add_format({'bold':True,'font_size':8,'font_name':'Arial'})
    big_font = workbook.add_format({'bold':True,'font_size':14,'font_name':'Arial'})
    normal_font = workbook.add_format({'bold':True,'font_size':10,'font_name':'Arial'})
    vertical_text_font_gray = workbook.add_format({'bold':True,'font_size':8,'border':1,'border_color':'black','bg_color':'gray','valign':'vjustify','align':'center','font_name':'Arial','rotation':90}) 
    horizontal_text_font_gray = workbook.add_format({'bold':True,'font_size':8,'border':1,'border_color':'black','bg_color':'gray','valign':'vcenter','align':'center','font_name':'Arial'}) 

    #Formatando a Data
    DATA_INICIAL_FORMATTED = DATA_INICIAL.strftime('%d/%m/%Y')
    DATA_FINAL_FORMATTED   = DATA_FINAL.strftime('%d/%m/%Y')

    #Nome da Planilha
    worksheet = workbook.add_worksheet("DS " + str(DISTRITO_SANITARIO))  

    #Informações da Prefeitura
    worksheet.insert_image("A1","prefeitura.jpg",{'x_scale': 0.28, 'y_scale': 0.39})
    worksheet.write("C1","Prefeitura do Recife",header_format)
    worksheet.write("C2","Secretaria de Saúde",header_format)
    worksheet.write("C3","Secretaria Executiva de Vigilância à Saúde",header_format)
    worksheet.write("C4","Gerência de Vigilância Ambiental e Controle de Zoonoses",header_format)
    worksheet.write("C5","Programa de Saúde Ambiental",header_format)
    worksheet.write("C6","Coordenação de Apoio Diagnóstico",header_format)

    #Informações sobre a Tabela(Distrito,Semestre,Data)
    worksheet.write("G8","Levantamento de Indice Rápido para Aedes aegypti",big_font)
    worksheet.write("B10","Distrito Sanitário:" + str(DISTRITO_SANITARIO),normal_font)
    worksheet.write("J10","LIRAa: "+ DATA_INICIAL_FORMATTED,normal_font)
    worksheet.write("S10","Período: " + DATA_INICIAL_FORMATTED + " à " + DATA_FINAL_FORMATTED,normal_font)


    #Cabeçalho da Tabela (Linha 1)
    worksheet.merge_range('A12:A14',"Estratos",vertical_text_font_gray)
    worksheet.merge_range('B12:I12',"Imóveis",horizontal_text_font_gray)
    worksheet.merge_range('J12:R12',"Tipos de Recipientes para Aedes aegypti",horizontal_text_font_gray)
    worksheet.merge_range('S12:S14',"Pend. (%) Informada",vertical_text_font_gray)
    worksheet.merge_range('T12:T14',"Recip. p/  Ae. Albop.",vertical_text_font_gray)
    worksheet.merge_range('U12:U14',"Imóv. à Rec.",vertical_text_font_gray)



    #Cabeçalho da Tabela (Linha 2)
    worksheet.merge_range('B13:B14',"Programados",horizontal_text_font_gray)
    worksheet.merge_range('C13:C14',"Inspecionados",horizontal_text_font_gray)
    worksheet.merge_range('D13:F13',"C/ Ae. aegypti",horizontal_text_font_gray)
    worksheet.merge_range('G13:I13',"C/ Ae. albopictus",horizontal_text_font_gray)
    worksheet.merge_range('J13:J14',"A1",horizontal_text_font_gray)
    worksheet.merge_range('K13:K14',"A2",horizontal_text_font_gray)
    worksheet.merge_range('L13:L14',"B",horizontal_text_font_gray)
    worksheet.merge_range('M13:M14',"C",horizontal_text_font_gray)
    worksheet.merge_range('N13:N14',"D1",horizontal_text_font_gray)
    worksheet.merge_range('O13:O14',"D2",horizontal_text_font_gray)
    worksheet.merge_range('P13:P14',"E",horizontal_text_font_gray)
    worksheet.merge_range('Q13:Q14',"Total",horizontal_text_font_gray)
    worksheet.merge_range('R13:R14',"Crítica",horizontal_text_font_gray)


    #Cabeçalho da Tabela (Linha 3)
    worksheet.write('D14',"TB",horizontal_text_font_gray)
    worksheet.write('E14',"Outros",horizontal_text_font_gray)
    worksheet.write('F14',"Total",horizontal_text_font_gray)
    worksheet.write('G14',"TB",horizontal_text_font_gray)
    worksheet.write('H14',"Outros",horizontal_text_font_gray)
    worksheet.write('I14',"Total",horizontal_text_font_gray)


    ########### Listas para armazenar valores que serão usados nas Tabela 1 e 2###
    ########### Isto evita o uso de laços desnecessarios e acessos ao Bando de Dados
    InspecionadosList = []
    TbAegyptiList     = []
    OutrosAegyptiList = []
    dicionarioFocosList = []
    totalFocosAegyptiList = []
    ImoveisPositivosAegyptiList = []

    for e in range (1,NUMERO_ESTRATOS+2):
        InspecionadosList.append(0)
        TbAegyptiList.append(0)
        OutrosAegyptiList.append(0)
        dicionarioFocosList.append(0)
        totalFocosAegyptiList.append(0)
        ImoveisPositivosAegyptiList.append(0)
    ###################################################################


    #########################################
    ##          Conteudo da tabela 1      ###
    #########################################

    #Imprime linhas para cada Estratos do Distrito + uma linha adicional com o Total 
    for numEstrato in range(1,NUMERO_ESTRATOS+2):
        #Para cada estrato do Distrito
        if (numEstrato <= NUMERO_ESTRATOS):
            #Dicionario com objetos selecionados entre a data requisitada
            queryDictList = VisitaFormularioLira.objects.filter(distritoSanitario=DISTRITO_SANITARIO,estrato=numEstrato,dataVisita__range=(DATA_INICIAL,DATA_FINAL)).values()
        else:
            #Linha TOTAL
            queryDictList= VisitaFormularioLira.objects.filter(distritoSanitario=DISTRITO_SANITARIO,dataVisita__range=(DATA_INICIAL,DATA_FINAL)).values()
            
        row = numEstrato + 13 # Linha onde começa as informações da Tabela (Diferente do numero da Linha exibida na Tabela)
        column = 0            # Coluna 'A'

        if (numEstrato <= NUMERO_ESTRATOS):
            worksheet.write(row,column,numEstrato)                  #Estratos
        else:
            worksheet.write(row,column,'Total')

        #-------Programados---------
        worksheet.write(row,column+1,-1)     

    
        #Captura de valores usados no Preenchimento das Tabelas
        Inspecionados = 0
        TbAegypti = 0
        ImoveisPositivosAegypti = 0
        for data in queryDictList:
            Inspecionados += 1     #Inspecionados
            if (data['tipoImovel'] == 4):
                TbAegypti += 1     #TB Ae. aegypti
            if (data['aedesA1']!=0 or data['aedesA2']!=0 or data['aedesB']!=0 or data['aedesC']!=0 or data['aedesD1']!=0 or data['aedesD2']!=0 or data['aedesE']!=0):
                ImoveisPositivosAegypti += 1 #IIP
        ImoveisPositivosAegyptiList.insert(numEstrato,ImoveisPositivosAegypti)


        #-------Inspecionados---------
        InspecionadosList.insert(numEstrato,Inspecionados)
        worksheet.write(row,column+2,Inspecionados)            

        #-------TB Ae. aegypti---------
        #Formularios realizados em Terrenos Baldios 
        TbAegyptiList.insert(numEstrato,TbAegypti)
        worksheet.write(row,column+3,TbAegypti)   

        #-------Outros Ae. aegypti---------
        #Formularios que NÃO foram realizados em Terrenos Baldios
        #OutrosAegypti = Inspecionados - TbAegypti
        OutrosAegypti = Inspecionados - TbAegypti
        OutrosAegyptiList.insert(numEstrato, OutrosAegypti)      
        worksheet.write(row,column+4,OutrosAegypti)  

        #-------Total Ae. aegypti---------
        TotalAegypti =  TbAegypti + OutrosAegypti
        worksheet.write(row,column+5,TotalAegypti)  

        #-------TB Ae. albopictus---------
        worksheet.write(row,column+6,0) 

        #-------Outros Ae. albopictus---------
        worksheet.write(row,column+7,0) 

        #-------Total Ae. albopictus---------
        worksheet.write(row,column+8,0)  

        #------Tipos de recipientes para Aedes aegypti---------
        #Calcula a soma de todos os tipos de recipientes encontrados 
        dicionarioFocos = {'aedesA1__sum':0,'aedesA2__sum':0,'aedesB__sum':0,'aedesC__sum':0,'aedesD1__sum':0,'aedesD2__sum':0,'aedesE__sum':0}
        for data in queryDictList:
            dicionarioFocos['aedesA1__sum'] += data['aedesA1']
            dicionarioFocos['aedesA2__sum'] += data['aedesA2']
            dicionarioFocos['aedesB__sum' ] += data['aedesB' ]
            dicionarioFocos['aedesC__sum' ] += data['aedesC' ]
            dicionarioFocos['aedesD1__sum'] += data['aedesD1']
            dicionarioFocos['aedesD2__sum'] += data['aedesD2']
            dicionarioFocos['aedesE__sum' ] += data['aedesE' ]

        dicionarioFocosList.insert(numEstrato,dicionarioFocos)

        

        #------Focos Aedes---------
        worksheet.write(row,column+9 ,dicionarioFocos['aedesA1__sum'])  #Focos A1
        worksheet.write(row,column+10,dicionarioFocos['aedesA2__sum'])  #Focos A2
        worksheet.write(row,column+11,dicionarioFocos['aedesB__sum'])   #Focos B
        worksheet.write(row,column+12,dicionarioFocos['aedesC__sum'])   #Focos C
        worksheet.write(row,column+13,dicionarioFocos['aedesD1__sum'])  #Focos D1
        worksheet.write(row,column+14,dicionarioFocos['aedesD2__sum'])  #Focos D2
        worksheet.write(row,column+15,dicionarioFocos['aedesE__sum'])   #Focos E

        #------Total tipos de recipientes---------
             #Não temos informações sobre o albopictus
             #Logo o total de recipientes deve ser igual ao Total Ae. aegypti
        totalFocosAegypti = 0
        for key in dicionarioFocos:
            totalFocosAegypti += dicionarioFocos[key]
        totalFocosAegyptiList.insert(numEstrato,totalFocosAegypti)
        worksheet.write(row,column+16,totalFocosAegypti)  

        #------Critica---------
            #Depende do Supervisor, ficará em branco
        worksheet.write(row,column+17,' ') 

        #------Recipientes para Albopictus---------
        worksheet.write(row,column+18,0) 

    
    #----------------------Fim da Tabela 1---------------------------


    ######################################################################
    ###    AQUI SE INICIA A IMPRESSÃO DA SEGUNDA TABELA DA PLANILHA    ###
    ######################################################################

    #Define a linha inicial da Tabela 2, que varia dependendo do número de estratos do Distrito
    initialRowTable2 = 13 + NUMERO_ESTRATOS + 3 

    #Cabeçalho da Tabela 2 (Linha 1)
    worksheet.merge_range(initialRowTable2,0,initialRowTable2+2,0,"Estratos",vertical_text_font_gray)
    worksheet.merge_range(initialRowTable2,1,initialRowTable2,6,"Imóveis",horizontal_text_font_gray)
    worksheet.merge_range(initialRowTable2,7,initialRowTable2,18,"Indicadores",horizontal_text_font_gray)


    #Cabeçalho da Tabela 2 (Linha 2)
    worksheet.merge_range(initialRowTable2+1,1,initialRowTable2+2,1,"Programados",horizontal_text_font_gray)
    worksheet.merge_range(initialRowTable2+1,2,initialRowTable2+2,2,"Inspecionados",horizontal_text_font_gray)
    worksheet.merge_range(initialRowTable2+1,3,initialRowTable2+1,4,"C/ Ae. aegypti (%)",horizontal_text_font_gray)
    worksheet.merge_range(initialRowTable2+1,5,initialRowTable2+1,6,"C/ Ae. albopictus (%)",horizontal_text_font_gray)
    worksheet.merge_range(initialRowTable2+1,7,initialRowTable2+2,7,"% Perda na amostragem",horizontal_text_font_gray)
    worksheet.merge_range(initialRowTable2+1,8,initialRowTable2+1,9,"I I P (%)",horizontal_text_font_gray)
    worksheet.merge_range(initialRowTable2+1,10,initialRowTable2+1,11,"Indice de Breteau",horizontal_text_font_gray)
    worksheet.merge_range(initialRowTable2+1,12,initialRowTable2+1,18,"ITR (Aedes aegypti) (%)",horizontal_text_font_gray)


    #Cabeçalho da Tabela 2 (Linha 3)
    worksheet.write(initialRowTable2+2,3,"TB",horizontal_text_font_gray)
    worksheet.write(initialRowTable2+2,4,"Outros",horizontal_text_font_gray)
    worksheet.write(initialRowTable2+2,5,"TB",horizontal_text_font_gray)
    worksheet.write(initialRowTable2+2,6,"Outros",horizontal_text_font_gray)
    worksheet.write(initialRowTable2+2,8,"Aegypti",horizontal_text_font_gray)
    worksheet.write(initialRowTable2+2,9,"Albopictus",horizontal_text_font_gray)
    worksheet.write(initialRowTable2+2,10,"Aegypti",horizontal_text_font_gray)
    worksheet.write(initialRowTable2+2,11,"Albopictus",horizontal_text_font_gray)
    worksheet.write(initialRowTable2+2,12,"A1",horizontal_text_font_gray)
    worksheet.write(initialRowTable2+2,13,"A2",horizontal_text_font_gray)
    worksheet.write(initialRowTable2+2,14,"B",horizontal_text_font_gray)
    worksheet.write(initialRowTable2+2,15,"C",horizontal_text_font_gray)
    worksheet.write(initialRowTable2+2,16,"D1",horizontal_text_font_gray)
    worksheet.write(initialRowTable2+2,17,"D2",horizontal_text_font_gray)
    worksheet.write(initialRowTable2+2,18,"E",horizontal_text_font_gray)


    #########################################
    ##          Conteudo da tabela 2      ###
    #########################################

    for numEstrato in range(1,NUMERO_ESTRATOS+2):
        contentRowTable2 = initialRowTable2 + 2 # Linha do conteundo da tabela 2
        
        if (numEstrato <= NUMERO_ESTRATOS):
            worksheet.write(contentRowTable2+numEstrato,0,numEstrato)                  #Estratos
        else:
            worksheet.write(contentRowTable2+numEstrato,0,'Total')

        #------Programados---------
        worksheet.write(contentRowTable2 + numEstrato,1,-1)          


        #------Inspecionados---------
        #Total de Formularios
        Inspecionados = InspecionadosList[numEstrato]
        worksheet.write(contentRowTable2 + numEstrato,2,Inspecionados)             


        #------TB Ae. aegypti (%)---------
        #Somente Terrenos Baldio
        TbAegypti = TbAegyptiList[numEstrato]
        TbAegyptiPorcentagem = 0
        if (Inspecionados != 0):
            TbAegyptiPorcentagem = round((TbAegypti/Inspecionados)*100,1)
        worksheet.write(contentRowTable2 + numEstrato,3,TbAegyptiPorcentagem)   

        #------Outros Ae. aegypti (%)---------
       #Qualquer Tipo de Imóvel que não seja Terreno Baldio
        OutrosAegypti = OutrosAegyptiList[numEstrato]
        OutrosAegyptiPorcentagem = 0
        if (Inspecionados != 0):
            OutrosAegyptiPorcentagem = round((OutrosAegypti/Inspecionados)*100,1)
        worksheet.write(contentRowTable2 + numEstrato,4,OutrosAegyptiPorcentagem)  


        #------TB Ae. aelbopictus (%)---------
        worksheet.write(contentRowTable2 + numEstrato,5,0)  

        #------Outros Ae. aelbopictus (%)---------
        worksheet.write(contentRowTable2 + numEstrato,6,0)  


        #------Perda na amostragem---------
        worksheet.write(contentRowTable2 + numEstrato,7,0)


        #------IIP Aegypti---------
            #Indicie Predial IIP (%) == (Imoveis Positivos/Inspecionados)*100 

        #Total de Imoveis onde existe pelo menos 1 tipo de foco            
        IIP = 0
        ImoveisPositivosAegypti = ImoveisPositivosAegyptiList[numEstrato]
        if (Inspecionados != 0):
            IIP = round((ImoveisPositivosAegypti/Inspecionados)*100,1)
        worksheet.write(contentRowTable2 + numEstrato,8,IIP)


        #------IIP Albopictus---------
        worksheet.write(contentRowTable2 + numEstrato,9,0)

        
        #------IB Aegypti------------
        #Indicie de Breteau IB  == (Recipientes positivos/Inspecionados)*100
        IBAegypti = 0
        if (Inspecionados != 0):    
            IBAegypti = round((totalFocosAegyptiList[numEstrato]/Inspecionados)*100,1)
        worksheet.write(contentRowTable2 + numEstrato,10,IBAegypti)
       
       
        #------IB Albopictus---------
        worksheet.write(contentRowTable2 + numEstrato,11,0)


        #----------Indicie de Tipos de Recipiente---------
            #Indicie de Tipos de Recipiente (%) ITR == (Recipiente positivos "X"/Total de Recipientes Positivos)*100
        dicionarioFocos = dicionarioFocosList[numEstrato]
        totalFocosAegypti = totalFocosAegyptiList[numEstrato]
        #------ITR Foco A1---------
        ITRAedesA1 = 0
        if (totalFocosAegypti != 0):  #Evita divisão por 0
            ITRAedesA1 = round((dicionarioFocos['aedesA1__sum']/totalFocosAegypti)*100,1)
        worksheet.write(contentRowTable2 + numEstrato,12,ITRAedesA1)

        #------ITR Foco A2---------
        ITRAedesA2 = 0
        if (totalFocosAegypti != 0): 
            ITRAedesA2 = round((dicionarioFocos['aedesA2__sum']/totalFocosAegypti)*100,1)
        worksheet.write(contentRowTable2 + numEstrato,13,ITRAedesA2)

        #------ITR Foco B---------
        ITRAedesB = 0
        if (totalFocosAegypti != 0): 
            ITRAedesB = round((dicionarioFocos['aedesB__sum']/totalFocosAegypti)*100,1)
        worksheet.write(contentRowTable2 + numEstrato,14,ITRAedesB)

        #------ITR Foco C---------
        ITRAedesC = 0
        if (totalFocosAegypti != 0): 
            ITRAedesC = round((dicionarioFocos['aedesC__sum']/totalFocosAegypti)*100,1)
        worksheet.write(contentRowTable2 + numEstrato,15,ITRAedesC)

        #------ITR Foco D1---------
        ITRAedesD1 = 0
        if (totalFocosAegypti != 0): 
            ITRAedesD1 = round((dicionarioFocos['aedesD1__sum']/totalFocosAegypti)*100,1)
        worksheet.write(contentRowTable2 + numEstrato,16,ITRAedesD1)

        #------ITR Foco D2---------
        ITRAedesD2 = 0
        if (totalFocosAegypti != 0): 
            ITRAedesD2 = round((dicionarioFocos['aedesD2__sum']/totalFocosAegypti)*100,1)
        worksheet.write(contentRowTable2 + numEstrato,17,ITRAedesD2)

        #------ITR Foco E---------
        ITRAedesE = 0
        if (totalFocosAegypti != 0): 
            ITRAedesE = round((dicionarioFocos['aedesE__sum']/totalFocosAegypti)*100,1)
        worksheet.write(contentRowTable2 + numEstrato,18,ITRAedesE)

        #---------------------------Fim da Tabela 2 ----------------------------------


    #Rodapé da Tabela
    #worksheet.merge_range('A32:T32',"Estratos",horizontal_text_font_gray)
