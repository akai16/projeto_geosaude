from django.test import TestCase, Client
from django.urls import reverse

from model_mommy import mommy
from django.contrib.auth import get_user_model
from django.conf import settings
from login.forms import UserEditForm

from api.models import VisitaFormularioLira

User = get_user_model()

class LoginViewTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        self.url = reverse('login_page')
        self.staff_member = mommy.prepare(User,is_staff=True)
        self.staff_member.set_password('teste123')
        self.staff_member.save()

    def tearDown(self):
        User.objects.all().delete()


    def test_view_ok(self):
        response = self.client.get(self.url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'login/login_pagina.html')

    def test_login_ok(self):
        data = {'username': self.staff_member.username, 'password': 'teste123'}
        response = self.client.post(self.url, data)   
        redirect_url = reverse(settings.LOGIN_REDIRECT_URL)
        self.assertRedirects(response, redirect_url)
        self.assertTrue(response.wsgi_request.user.is_authenticated)
    
    
class CadastroViewTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        self.url = reverse('cadastro')
        self.staff_member = mommy.prepare(User, is_staff=True)
        self.staff_member.set_password('teste123')
        self.staff_member.save()
        self.client.login(username=self.staff_member.username, password='teste123')

    def tearDown(self):
        User.objects.all().delete()


    def test_view_ok(self):
        response = self.client.get(self.url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'login/cadastro.html')

    def test_register_ok(self):
        data = { 
            'username': 'fulano', 'nome': 'Fulando de Tal', 'profissao': 'Teste', 'posto': '1', 'regiao': 'Candeias',
            'password1': 'teste1234', 'password2': 'teste1234'
        }
        response = self.client.post(self.url, data)
        redirect_url = reverse('usuario')
        self.assertRedirects(response, redirect_url)
        self.assertEquals(User.objects.filter(is_staff=False).count(), 1)
    

class EditarUsuarioViewTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        self.staff_member = mommy.prepare(User, is_staff=True)
        self.staff_member.set_password('teste123')
        self.staff_member.save()
        self.client.login(username=self.staff_member.username, password='teste123')
        self.user = mommy.make(User)
        self.url = reverse('editar_usuario', kwargs={'userId': self.user.id})

    def tearDown(self):
        User.objects.all().delete()


    def test_view_ok(self):
        response = self.client.get(self.url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'login/editar_usuario.html')

    def test_edit_ok(self):
        self.assertNotEquals(self.user.nome, 'Cicrano')
        data = {
            'nome': 'Cicrano', 'profissao': self.user.profissao , 'posto': self.user.posto, 'regiao': self.user.regiao
        }
        response = self.client.post(self.url, data)
        redirect_url = reverse('usuario')
        self.assertRedirects(response, redirect_url)
        self.assertEquals(User.objects.get(username=self.user.username).nome, 'Cicrano')


class LiraAListViewTestCase(TestCase):
    
    def setUp(self):
        self.client = Client()
        self.staff_member = mommy.prepare(User, is_staff=True)
        self.staff_member.set_password('teste123')
        self.staff_member.save()
        self.client.login(username=self.staff_member.username, password='teste123')
        self.user = mommy.make(User)
        self.url = reverse('liraa_lista', kwargs={'userId': self.user.id})
        self.formularios = mommy.make(VisitaFormularioLira,agenteDeSaude=self.user, _quantity=27)

    def tearDown(self):
        User.objects.all().delete()
        VisitaFormularioLira.objects.all().delete()


    def test_view_ok(self):
        response = self.client.get(self.url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'login/formularios.html')

    def test_context_ok(self):
        response = self.client.get(self.url)
        self.assertTrue('formulario_list' in response.context)
        self.assertTrue('formulario' in response.context)
        self.assertEquals(response.context['paginator'].num_pages,2)


class ConsolidadosPageTestCase(TestCase):
    
    def setUp(self):
        self.client = Client()
        self.url = reverse('consolidados')
        self.staff_member = mommy.prepare(User, is_staff=True)
        self.staff_member.set_password('teste123')
        self.staff_member.save()
        self.client.login(username=self.staff_member.username, password='teste123')

    def tearDown(self):
        User.objects.all().delete()


    def test_view_ok(self):
        response = self.client.get(self.url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'login/consolidados.html')

    def test_context_ok(self):
        response = self.client.get(self.url)
        self.assertTrue('success' in response.context)
        self.assertTrue('consolidadosForm' in response.context)

    def test_data_input_fail(self):
        data = { 'dataInicial': '11.00.0000', 'dataFinal': '11/01/2012'}
        response = self.client.post(self.url, data)
        self.assertFormError(response, 'consolidadosForm', 'dataInicial', 'Informe uma data válida no formato dd/mm/aaaa.')