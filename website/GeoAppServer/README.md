# GAppServer
The Web Server uses Django and MongoDB
Make sure you have MongoDB 3.4 or higher 

#Criando
pip install -r requirements.txt
python manage.py makemigrations
python manage.py migrate

#Populando Banco De Dados
python manage.py shell
exec(open('MongoScript.py').read())
quit()

#Rodando Servidor
python manage.py runserver

O Site pode ser acessado em: http://localhost:8000/

#Para criar um Usuario capaz de acessar a área do Administrador:
python manage.py createsuperuser
