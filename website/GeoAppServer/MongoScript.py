from api.models import DistritoSanitario, Bairros
from datetime import date

#Este Script tem como função popular o banco de dados.
# 
#Os Distritos Sanitarios e Bairros são necessárias
#para armazenar de forma correta os Formularios LiraA e PSA.


# O Script deve ser usado apenas 1 vez , depois das Migrações do Django
# python manage.py shell
# exec(open('MongoScript.py').read())
# quit()

#Criando Distritos Sanitarios
distrito1 = DistritoSanitario(numDistrito=1,qntEstratos=6)
distrito1.save()

distrito2 = DistritoSanitario(numDistrito=2,qntEstratos=15)
distrito2.save()

distrito3 = DistritoSanitario(numDistrito=3,qntEstratos=7)
distrito3.save()

distrito4 = DistritoSanitario(numDistrito=4,qntEstratos=13)
distrito4.save()

distrito5 = DistritoSanitario(numDistrito=5,qntEstratos=16)
distrito5.save()

distrito6 = DistritoSanitario(numDistrito=6,qntEstratos=6)
distrito6.save()

distrito7 = DistritoSanitario(numDistrito=7,qntEstratos=12)
distrito7.save()

distrito8 = DistritoSanitario(numDistrito=8,qntEstratos=9)
distrito8.save()

#Criando Bairros
Bairros(nomeBairro="Rua Marquês de Baipendi",data=date.today(),idEstrato=16,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Rua Cirilino Afonso de Melo",data=date.today(),idEstrato=16,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Rua Mário Sete",data=date.today(),idEstrato=16,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Rua São Caetano",data=date.today(),idEstrato=16,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Av. Agamenon Magalhães",data=date.today(),idEstrato=1,distritoSanitario=distrito3).save()
Bairros(nomeBairro="Rua Guaianazes",data=date.today(),idEstrato=16,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Rua Esberard",data=date.today(),idEstrato=16,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Rua Pereira Passos",data=date.today(),idEstrato=16,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Rua Nova",data=date.today(),idEstrato=1,distritoSanitario=distrito1).save()
Bairros(nomeBairro="Rua Projetada",data=date.today(),idEstrato=16,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Rua Ledinha",data=date.today(),idEstrato=16,distritoSanitario=distrito2).save()


#Bairros do Distrito Sanitario I
Bairros(nomeBairro="Santo Antônio e Bairro do Recife",data=date.today(),idEstrato=1,distritoSanitario=distrito1).save()
Bairros(nomeBairro="Coelhos, Ilha do Leite e Paissandu",data=date.today(),idEstrato=2,distritoSanitario=distrito1).save()
Bairros(nomeBairro="Boa Vista e Soledade",data=date.today(),idEstrato=3,distritoSanitario=distrito1).save()
Bairros(nomeBairro="São José e Cabanga",data=date.today(),idEstrato=4,distritoSanitario=distrito1).save()
Bairros(nomeBairro="Joana Bezerra",data=date.today(),idEstrato=5,distritoSanitario=distrito1).save()
Bairros(nomeBairro="Santo Amaro",data=date.today(),idEstrato=6,distritoSanitario=distrito1).save()


#Bairros do Distrito Sanitario II
Bairros(nomeBairro="Dois Unidos-Área 1",data=date.today(),idEstrato=1,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Beberibe",data=date.today(),idEstrato=2,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Linha do Tiro",data=date.today(),idEstrato=3,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Água Fria-Área 1",data=date.today(),idEstrato=4.distritoSanitario=distrito2).save()
Bairros(nomeBairro="Água Fria-Área 2",data=date.today(),idEstrato=5,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Porto da Madeira",data=date.today(),idEstrato=6,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Cajueiro",data=date.today(),idEstrato=7,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Fundão",data=date.today(),idEstrato=8,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Alto Stª Terezinha",data=date.today(),idEstrato=9,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Bomba do Hemetério",data=date.today(),idEstrato=10,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Campina do Barreto",data=date.today(),idEstrato=11,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Peixinhos",data=date.today(),idEstrato=12,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Arruda e Ponto de Parada",data=date.today(),idEstrato=13,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Encruzilhada, Torreão, Rosarinho e Hipódromo",data=date.today(),idEstrato=14,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Dois Unidos-Área 2",data=date.today(),idEstrato=15,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Campo Grande",data=date.today(),idEstrato=16,distritoSanitario=distrito2).save()


#Bairros do Distrito Sanitario III
Bairros(nomeBairro="Derby, Graças, Espinheiro e Aflitos",data=date.today(),idEstrato=1,distritoSanitario=distrito3).save()
Bairros(nomeBairro="Jaqueira, Parnamirim e Tamarineira",data=date.today(),idEstrato=2,distritoSanitario=distrito3).save()
Bairros(nomeBairro="Monteiro e Apipucos",data=date.today(),idEstrato=3,distritoSanitario=distrito3).save()
Bairros(nomeBairro="Casa Forte, Poço da Panela e Santana",data=date.today(),idEstrato=4,distritoSanitario=distrito3).save()
Bairros(nomeBairro="Sítio dos Pintos e Dois Irmãos",data=date.today(),idEstrato=5,distritoSanitario=distrito3).save()
Bairros(nomeBairro="Alto do Mandu",data=date.today(),idEstrato=6,distritoSanitario=distrito3).save()
Bairros(nomeBairro="Casa Amarela",data=date.today(),idEstrato=7,distritoSanitario=distrito3).save()


#Bairros do Distrito Sanitario IV
Bairros(nomeBairro="Ilha do Retiro e Madalena",data=date.today(),idEstrato=1,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Cordeiro 1 e Zumbi",data=date.today(),idEstrato=2,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Cordeiro 2",data=date.today(),idEstrato=3,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Torre",data=date.today(),idEstrato=4,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Prado",data=date.today(),idEstrato=5,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Torrôes",data=date.today(),idEstrato=6,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Engenho do Meio",data=date.today(),idEstrato=7,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Iputinga 1",data=date.today(),idEstrato=8,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Várzea 1 (CDU) / Várzea (Brasilit) / CDU",data=date.today(),idEstrato=9,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Várzea 3 (Baixo) / Várzea 4 (Barreiras)",data=date.today(),idEstrato=10,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Várzea 5 (UR-7)",data=date.today(),idEstrato=11,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Caxangá",data=date.today(),idEstrato=12,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Iputinga 2",data=date.today(),idEstrato=13,distritoSanitario=distrito4).save()


#Bairros do Distrito Sanitario V
Bairros(nomeBairro="Afogados 1",data=date.today(),idEstrato=1,distritoSanitario=distrito5).save()
Bairros(nomeBairro="Afogados 2",data=date.today(),idEstrato=2,distritoSanitario=distrito5).save()
Bairros(nomeBairro="Bongi",data=date.today(),idEstrato=3,distritoSanitario=distrito5).save()
Bairros(nomeBairro="San Martin",data=date.today(),idEstrato=4,distritoSanitario=distrito5).save()
Bairros(nomeBairro="Mustardinha",data=date.today(),idEstrato=5,distritoSanitario=distrito5).save()
Bairros(nomeBairro="Magueira",data=date.today(),idEstrato=6,distritoSanitario=distrito5).save()
Bairros(nomeBairro="Estância",data=date.today(),idEstrato=7,distritoSanitario=distrito5).save()
Bairros(nomeBairro="Jiquiá",data=date.today(),idEstrato=8,distritoSanitario=distrito5).save()
Bairros(nomeBairro="Caçote",data=date.today(),idEstrato=9,distritoSanitario=distrito5).save()
Bairros(nomeBairro="Areias",data=date.today(),idEstrato=10,distritoSanitario=distrito5).save()
Bairros(nomeBairro="Tejipió",data=date.today(),idEstrato=11,distritoSanitario=distrito5).save()
Bairros(nomeBairro="Sancho",data=date.today(),idEstrato=12,distritoSanitario=distrito5).save()
Bairros(nomeBairro="Barro",data=date.today(),idEstrato=13,distritoSanitario=distrito5).save()
Bairros(nomeBairro="Coqueiral",data=date.today(),idEstrato=14,distritoSanitario=distrito5).save()
Bairros(nomeBairro="Curado e Totó",data=date.today(),idEstrato=15,distritoSanitario=distrito5).save()
Bairros(nomeBairro="Jardim São Paulo",data=date.today(),idEstrato=16,distritoSanitario=distrito5).save()


#Bairros do Distrito Sanitario VI
Bairros(nomeBairro="Boa Viagem",data=date.today(),idEstrato=1,distritoSanitario=distrito6).save()
Bairros(nomeBairro="Brasília Teimosa",data=date.today(),idEstrato=2,distritoSanitario=distrito6).save()
Bairros(nomeBairro="Imbiribeira 1",data=date.today(),idEstrato=3,distritoSanitario=distrito6).save()
Bairros(nomeBairro="Pina",data=date.today(),idEstrato=4,distritoSanitario=distrito6).save()
Bairros(nomeBairro="Ipsep",data=date.today(),idEstrato=5,distritoSanitario=distrito6).save()
Bairros(nomeBairro="Imbiribeira 2",data=date.today(),idEstrato=6,distritoSanitario=distrito6).save()


#Bairros do Distrito Sanitario VII
Bairros(nomeBairro="Alto José Bonifácio",data=date.today(),idEstrato=1,distritoSanitario=distrito7).save()
Bairros(nomeBairro="Alto José do Pinho",data=date.today(),idEstrato=2,distritoSanitario=distrito7).save()
Bairros(nomeBairro="Brejo de Beberibe",data=date.today(),idEstrato=3,distritoSanitario=distrito7).save()
Bairros(nomeBairro="Brejo de Guabiraba",data=date.today(),idEstrato=4,distritoSanitario=distrito7).save()
Bairros(nomeBairro="Córrego do Jenipapo",data=date.today(),idEstrato=5,distritoSanitario=distrito7).save()
Bairros(nomeBairro="Guabiraba e Pau Ferro",data=date.today(),idEstrato=6,distritoSanitario=distrito7).save()
Bairros(nomeBairro="Macaxeira",data=date.today(),idEstrato=7,distritoSanitario=distrito7).save()
Bairros(nomeBairro="Mangabeira",data=date.today(),idEstrato=8,distritoSanitario=distrito7).save()
Bairros(nomeBairro="Morro da Conceição",data=date.today(),idEstrato=9,distritoSanitario=distrito7).save()
Bairros(nomeBairro="Nova Descoberta",data=date.today(),idEstrato=10,distritoSanitario=distrito7).save()
Bairros(nomeBairro="Passarinho",data=date.today(),idEstrato=11,distritoSanitario=distrito7).save()
Bairros(nomeBairro="Vasco da Gama",data=date.today(),idEstrato=12,distritoSanitario=distrito7).save()


#Bairros do Distrito Sanitario VII
Bairros(nomeBairro="IBURA 1",data=date.today(),idEstrato=1,distritoSanitario=distrito8).save()
Bairros(nomeBairro="IBURA 2",data=date.today(),idEstrato=2,distritoSanitario=distrito8).save()
Bairros(nomeBairro="IBURA 3",data=date.today(),idEstrato=3,distritoSanitario=distrito8).save()
Bairros(nomeBairro="JORDÃO",data=date.today(),idEstrato=4,distritoSanitario=distrito8).save()
Bairros(nomeBairro="COHAB 1",data=date.today(),idEstrato=5,distritoSanitario=distrito8).save()
Bairros(nomeBairro="COHAB 2",data=date.today(),idEstrato=6,distritoSanitario=distrito8).save()
Bairros(nomeBairro="COHAB 3",data=date.today(),idEstrato=7,distritoSanitario=distrito8).save()
Bairros(nomeBairro="COHAB 4",data=date.today(),idEstrato=8,distritoSanitario=distrito8).save()
Bairros(nomeBairro="COHAB 5",data=date.today(),idEstrato=9,distritoSanitario=distrito8).save()


