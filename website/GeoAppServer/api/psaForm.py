from django.db import models
from login.models import AgenteDeSaude 
from api.models import Bairros

#Populando nova Instancia a partir do JSON
def populatePSAForm (PSAFormInstance,dataJSON,agente):
      
    #Erro lançado caso uma destas informações não esteja presente
    if 'places' not in dataJSON:
        return False
    if 'immobile' not in dataJSON:
        return False
    if 'situationImmobile' not in dataJSON['immobile']:
        return False
    if 'dateVisit' not in dataJSON['immobile']:
        return False
    if 'tpImmobile' not in dataJSON['immobile']:
        return False

    #Informações sobre o Imovel
    PSAFormInstance.localImovel = dataJSON['places']
    PSAFormInstance.dataVisita = dataJSON['immobile']['dateVisit']

    #Deriva codigo do Tipo de Imóvel
    immobileCode = immobileType(dataJSON['immobile']['tpImmobile'])
    if immobileCode == -1:
        return False
    PSAFormInstance.tipoImovel = immobileCode
    
    #Deriva codigo do Tipo da Situação do Imóvel
    situationCode = situationType(dataJSON['immobile']['situationImmobile'])
    if situationCode == -1:
        return False
    PSAFormInstance.situacaoImovel = situationCode
    

    #Identificação da Rua e Endereço
    rua, endereco = dataJSON['places'].split(',') 
    
    #Procura informações sobre a rua/bairro 
    #Devido a possibilidade de um bairro/rua mudar de Estrato, armazenamos varios dados do mesmo Bairro em datas diferentes.
    #Iremos procurar a linha cuja data seja mais proxima da data do Formulario  
    bairroObject = Bairros.objects.filter(nomeBairro = rua,data__lte=dataJSON['immobile']['dateVisit']).order_by('-data').first()
    #Caso a data do formulario seja mais antiga que qualquer registro do BD, iremos usar o registro mais antigo
    if bairroObject is None:
        bairroObject = Bairros.objects.filter(nomeBairro = rua).order_by('-data').first();
   
    #Distrito Sanitario e Estrato
    PSAFormInstance.estrato = bairroObject.idEstrato
    PSAFormInstance.distritoSanitario = bairroObject.distritoSanitario.numDistrito

    #Agente de Saúde Responsavel
    PSAFormInstance.agenteDeSaude = agente

    #Lixo
    #Deriva codigo do Tipo de Imóvel
    trashTypeCode = trashType(dataJSON['trash']['tpTrash'])
    if trashTypeCode == -1:
        return False
    PSAFormInstance.lixoTipo = trashTypeCode

    #Deriva codigo do Tipo de Imóvel
    trashPackagingCode = trashPackagingType(dataJSON['trash']['packaging'])
    if trashPackagingCode == -1:
        return False
    PSAFormInstance.lixoSituacao = trashPackagingCode
    
    #Deriva codigo do Tipo de Imóvel
    trashDestinyCode = trashDestinyType(dataJSON['trash']['domicileTrashDestiny'])
    if trashDestinyCode == -1:
        return False
    PSAFormInstance.lixoTratamento = trashDestinyCode



    #Vetor Aedes
    if 'vtAedes' in dataJSON and dataJSON['vtAedes'] is not None: #Checa a existência da chave no Dicionario
        if 'tpBreedingGrounds' in dataJSON['vtAedes']  and dataJSON['vtAedes']['tpBreedingGrounds'] is not None:
            if 'A1' in dataJSON['vtAedes']['tpBreedingGrounds']:
                PSAFormInstance.aedesA1 = dataJSON['vtAedes']['tpBreedingGrounds']['A1'] 
            if 'A2' in dataJSON['vtAedes']['tpBreedingGrounds']:
                PSAFormInstance.aedesA2 = dataJSON['vtAedes']['tpBreedingGrounds']['A2']
            if 'B' in dataJSON['vtAedes']['tpBreedingGrounds']:
                PSAFormInstance.aedesB = dataJSON['vtAedes']['tpBreedingGrounds']['B']
            if 'C' in dataJSON['vtAedes']['tpBreedingGrounds']:
                PSAFormInstance.aedesC = dataJSON['vtAedes']['tpBreedingGrounds']['C']
            if 'D1' in dataJSON['vtAedes']['tpBreedingGrounds']:
                PSAFormInstance.aedesD1 = dataJSON['vtAedes']['tpBreedingGrounds']['D1']
            if 'D2' in dataJSON['vtAedes']['tpBreedingGrounds']:
                PSAFormInstance.aedesD2 = dataJSON['vtAedes']['tpBreedingGrounds']['D2']
            if 'E' in dataJSON['vtAedes']['tpBreedingGrounds']:
                PSAFormInstance.aedesE = dataJSON['vtAedes']['tpBreedingGrounds']['E']
        
        if 'tpTreatment' in dataJSON['vtAedes']  and dataJSON['vtAedes']['tpTreatment'] is not None:
            if 'positive' in dataJSON['vtAedes']['tpTreatment']:
                PSAFormInstance.aedesPositivo = dataJSON['vtAedes']['tpTreatment']['positive']
            if 'mechanical' in dataJSON['vtAedes']['tpTreatment']:
                PSAFormInstance.aedesMecanico = dataJSON['vtAedes']['tpTreatment']['mechanical'] 
            if 'biological' in dataJSON['vtAedes']['tpTreatment']:
                PSAFormInstance.aedesBiologico = dataJSON['vtAedes']['tpTreatment']['biological'] 
            if 'chemical' in dataJSON['vtAedes']['tpTreatment']:
                PSAFormInstance.aedesQuimico = dataJSON['vtAedes']['tpTreatment']['chemical'] 
    

    #Vetor Culex
    if 'vtCulex' in dataJSON and dataJSON['vtCulex'] is not None:
        if 'tpBreedingGrounds' in dataJSON['vtCulex'] and dataJSON['vtCulex']['tpBreedingGrounds'] is not None:
            if 'fosse'in dataJSON['vtCulex']['tpBreedingGrounds']:
                PSAFormInstance.culexFosso = dataJSON['vtCulex']['tpBreedingGrounds']['fosse']
            if 'cistern'in dataJSON['vtCulex']['tpBreedingGrounds']:
                PSAFormInstance.culexCisterna = dataJSON['vtCulex']['tpBreedingGrounds']['cistern']
            if 'channelDichGutters'in dataJSON['vtCulex']['tpBreedingGrounds']:
                PSAFormInstance.culexCanal = dataJSON['vtCulex']['tpBreedingGrounds']['channelDichGutters']
            if 'puddle'in dataJSON['vtCulex']['tpBreedingGrounds']:
                PSAFormInstance.culexCharco = dataJSON['vtCulex']['tpBreedingGrounds']['puddle']
            if 'inspectionBoxDrainage'in dataJSON['vtCulex']['tpBreedingGrounds']:
                PSAFormInstance.culexDrenagem = dataJSON['vtCulex']['tpBreedingGrounds']['inspectionBoxDrainage']

        if 'tpTreatment' in dataJSON['vtCulex'] and dataJSON['vtCulex']['tpTreatment'] is not None:
            if 'positive' in dataJSON['vtCulex']['tpTreatment']:
                PSAFormInstance.culexPositivo = dataJSON['vtCulex']['tpTreatment']['positive']
            if 'mechanical' in dataJSON['vtCulex']['tpTreatment']:
                PSAFormInstance.culexMecanico = dataJSON['vtCulex']['tpTreatment']['mechanical'] 
            if 'biological' in dataJSON['vtCulex']['tpTreatment']:
                PSAFormInstance.culexBiologico = dataJSON['vtCulex']['tpTreatment']['biological'] 
            if 'chemical' in dataJSON['vtCulex']['tpTreatment']:
                PSAFormInstance.culexQuimico = dataJSON['vtCulex']['tpTreatment']['chemical']


    #Ovitrampas
    if 'ovitraps' in dataJSON  and dataJSON['ovitraps'] is not None:
        PSAFormInstance.ovitrampas = dataJSON['ovitraps']

    
    #Larvicidas
    if 'larvicides' in dataJSON and dataJSON['larvicides'] is not None:
        if 'BTiG_depositos' in dataJSON['larvicides']:
            PSAFormInstance.larvicidaBTiG_depositos =  dataJSON['larvicides']['BTiG_depositos']
        if 'BTiG_gramas' in dataJSON['larvicides']:
            PSAFormInstance.larvicidaBTiG_gramas    =  dataJSON['larvicides']['BTiG_gramas']
        if 'BTiWDg_depositos' in dataJSON['larvicides']:
            PSAFormInstance.larvicidaBTiWDg_depositos = dataJSON['larvicides']['BTiWDg_depositos']
        if 'BTiWDg_gramas' in dataJSON['larvicides']:    
            PSAFormInstance.larvicidaBTiWDg_gramas  =  dataJSON['larvicides']['BTiWDg_gramas']
        if 'BsG_depositos' in dataJSON['larvicides']:
            PSAFormInstance.larvicidaBTiWDg_depositos = dataJSON['larvicides']['BsG_depositos']
        if 'BsG_gramas' in dataJSON['larvicides']:    
            PSAFormInstance.larvicidaBTiWDg_gramas = dataJSON['larvicides']['BsG_gramas']


    return PSAFormInstance


#As funções abaixo mapeiam os valores presente no JSON para a legenda usada pela Prefeitura
def immobileType(type):
    if type == 'residence':
        return 1
    elif type == 'commerce':
        return 2
    elif type == 'private service':
        return 3
    elif type == 'industry':
        return 4
    elif type == 'residence with commerce':
        return 5
    elif type == 'immobile under construction':
        return 6
    elif type == 'wasteland':
        return 7
    elif type == 'strategic point':
        return 8
    elif type == 'others':
        return 9    
    else:
        return -1

def situationType(type):
    if type == 'inspected':
        return 1
    elif type == 'not inspected':
        return 2
    elif type == 'recovered':
        return 3
    else:
        return -1

def trashType(type):
    if type == 'domicile':
        return 1
    elif type == 'special':
        return 2
    else:
        return -1

def trashPackagingType(type):
    if type == 'appropriate':
        return 1
    elif type == 'inappropriate':
        return 2
    elif type == 'not applicable':
        return 3
    else:
        return -1

def trashDestinyType(type):
    if type == 'directly collect':
        return 1
    elif type == 'indirectly collect':
        return 2
    elif type == 'burned and buried':
        return 3
    elif type == 'throw on barrier':
        return 4
    elif type == 'throw on wasteland':
        return 5
    elif type == 'throw on the river, lake, sea or mangrove':
        return 6
    elif type == 'throw in the gallery, duct or channel':
        return 7
    elif type == 'other':
        return 8
    elif type == 'selective collect':
        return 9
    elif type == 'not applicable':
        return 10
    else:
        return -1